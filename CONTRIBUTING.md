Glad to see all your members!

This project is open for students in Ben's Computational Thinking class to upload their codes of nand2tetris.

Contributors should name your codes with name-student ID-file name and push that into the "homework" repository:

1.In the homework repository you can find the correct term you are current in.

2.Create a new repository in the repository of your term (currently: 2016 Autumn Cimputational Thinking) with your name and student ID.

3.Push your projects into your repository, each file should be named as "Your_name-projectx"

4.Good luck!